## ORID homework


	O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?
	R (Reflective): Please use one word to express your feelings about today's class.
	I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?
	D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?


- O (Objective): learned about three topics related to Spring Boot: three-tier architecture, unit testing, and exception handling methods. We engaged in discussions and activities to understand these concepts better.

- R (Reflective): Enlightened.

- I (Interpretive): Today's class was quite informative and enriching. Understanding the three-tier architecture in Spring Boot gave us insights into how to structure our applications to achieve better separation of concerns and maintainability. Learning about unit testing in Spring Boot was valuable as it helps ensure the reliability of our code and gives us confidence in making changes without fear of breaking existing functionality. Additionally, the exception handling methods taught us how to gracefully handle errors and provide meaningful feedback to users, enhancing the overall user experience.

- D (Decisional): I am eager to apply what I've learned today in my upcoming Spring Boot projects. Implementing a three-tier architecture will allow me to organize my code more efficiently and make future maintenance easier. I am excited to start incorporating unit testing into my development process to ensure code quality and catch issues early on. 