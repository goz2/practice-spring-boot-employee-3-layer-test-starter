package com.afs.restapi.service;

import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public EmployeeRepository getEmployeeRepository() {
        return employeeRepository;
    }

    public List<Employee> getAll() {
        return employeeRepository.findAll();
    }

    public Employee getById(int id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> getByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> getByPage(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee saveEmployee(Employee employee) {
        if (!employee.ageMatch()) {
            throw new AgeErrorException();
        }
        if (!employee.ageAndSalaryMatch()) {
            throw new AgeErrorException();
        }
        return employeeRepository.insert(employee);
    }

    public Employee editEmployee(int id, Employee employee) {
        Employee currentEmployee = employeeRepository.findById(id);
        if (!currentEmployee.getStatus()) {
            throw new EmployeeNotFoundException();
        }
        return employeeRepository.update(id, employee);
    }

    public void deleteEmployeeById(int id) {
        Employee employee = employeeRepository.findById(id);
        employee.setStatus(false);
        employeeRepository.update(id, employee);
    }
}