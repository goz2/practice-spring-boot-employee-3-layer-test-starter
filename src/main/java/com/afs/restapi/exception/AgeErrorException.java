package com.afs.restapi.exception;

public class AgeErrorException extends RuntimeException{
    public AgeErrorException() {
        super("age error.");
    }
}
