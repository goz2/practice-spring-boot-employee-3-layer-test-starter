package com.afs.restapi.model;

public class Employee {
    public static final int LOW_LIMIT_AGE = 18;
    public static final int HIGH_LIMIT_AGE = 65;
    public static final int AGE_30 = 30;
    public static final int SALARY_20000 = 20000;
    private int id;
    private String name;
    private int age;
    private String gender;
    private int salary;

    private boolean status = true;

    public Employee() {
    }

    public Employee(int id, String name, int age, String gender, int salary) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.salary = salary;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public void merge(Employee employee) {
        this.salary = employee.salary;
    }

    public boolean ageMatch() {
        return getAge() >= LOW_LIMIT_AGE && getAge() <= HIGH_LIMIT_AGE;
    }

    public boolean ageAndSalaryMatch() {
        return !(getAge() > AGE_30 && getSalary() < SALARY_20000);
    }
}
