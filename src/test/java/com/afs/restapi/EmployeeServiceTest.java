package com.afs.restapi;

import com.afs.restapi.exception.AgeErrorException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class EmployeeServiceTest {
    EmployeeRepository repo = mock(EmployeeRepository.class);
    EmployeeService service = new EmployeeService(repo);

    @Test
    void should_throw_age_error_exception_when_perform_insert_employee_given_employee_with_age_15_and_age_66() {
        Employee employee15 = new Employee(1, "p1", 15, "female", 800);
        Employee employee66 = new Employee(1, "p1", 66, "male", 800);

        Assertions.assertThrows(AgeErrorException.class, () -> service.saveEmployee(employee15));
        Assertions.assertThrows(AgeErrorException.class, () -> service.saveEmployee(employee66));
    }

    @Test
    void should_throw_age_error_exception_when_perform_insert_employee_given_employee_salary_lower_20000_with_age_over_30() {
        Employee employee35 = new Employee(1, "p1", 35, "female", 10000);

        Assertions.assertThrows(AgeErrorException.class, () -> service.saveEmployee(employee35));
    }

    @Test
    void should_change_employ_set_status_inactive_default_when_perform_delete_employee_given_employee_id() {
        // given
        Employee employee = new Employee(1, "p1", 35, "female", 30000);
        Employee employee1 = new Employee(1, "p1", 35, "female", 30000);
        repo.insert(employee);
        when(repo.findById(anyInt())).thenReturn(employee1);

        // when
        service.deleteEmployeeById(employee.getId());

        // then
        Assertions.assertFalse(employee1.getStatus());

        verify(repo).update(anyInt(), argThat(currentEmployee -> {
            Assertions.assertFalse(currentEmployee.getStatus());
            return true;
        }));
    }

    @Test
    void should_change_employ_info_when_perform_put_employee_given_employee() {
        // given
        Employee employee = new Employee(1, "p1", 35, "female", 30000);
        Employee returnEmployee = new Employee(1, "p1", 36, "female", 32000);
        Employee editEmployeeInfo = new Employee();
        editEmployeeInfo.setSalary(32000);
        repo.insert(employee);
        when(repo.findById(anyInt())).thenReturn(employee);
        when(repo.update(anyInt(), any())).thenReturn(returnEmployee);

        // when
        Employee newEmployee = service.editEmployee(employee.getId(), editEmployeeInfo);

        // then
        Assertions.assertEquals(32000, newEmployee.getSalary());

        verify(repo).update(anyInt(), argThat(currentEmployee -> {
            Assertions.assertEquals(32000, currentEmployee.getSalary());
            return true;
        }));
    }

    @Test
    void should_save_employ_set_status_active_default_when_perform_insert_employee_given_employee() {
        // given
        Employee employee = new Employee(1, "p1", 35, "female", 30000);
        Employee returnEmployee = new Employee(1, "p1", 35, "female", 30000);
        returnEmployee.setStatus(true);
        when(repo.insert(any())).thenReturn(returnEmployee);

        // when
        Employee newEmployee = service.saveEmployee(employee);

        // then
        Assertions.assertTrue(newEmployee.getStatus());

        verify(repo).insert(argThat(currentEmployee -> {
            Assertions.assertTrue(currentEmployee.getStatus());
            return true;
        }));
    }

}
